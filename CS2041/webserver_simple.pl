#! /usr/bin/perl -w

use IO::Socket;
$server = IO::Socket::INET->new(LocalPort => 2041, ReuseAddr =>1, Listen => SOMAXCONN) or die;
print "Access this server at http://localhost:2041/\n\n";
while ($c =  $server->accept()){
	my $request = <$c>;
	print "Connection from ", $c->peerhost, ":$request";
	print $c "HTTP/1.0 200 OK\n\n";
	$request =~ /^GET (.+) HTTP\/1.[01]\s*S/;
	if (open F, "</var/www/$1"){
		print $c <F>;
	}
	close $c;
}
