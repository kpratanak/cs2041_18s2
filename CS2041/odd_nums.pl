#! /usr/bin/perl -w

sub print_selected{
	my($select_expression,  @list) = @_;
	
	foreach $_ (@list){
		print "$n\n" if &$select_expression; #the & tells perl to execute the expression
	}
#	foreach $n (@numbers){
#		print "$n\n" if $n % 2 == 1;
#	}

	#or
	#$_% 2 and print "$_\n" foreach @_;

	#or
	#
}

print_selected(sub{$_ % 2 == 1},1..100);
print_selected(sub{$_ >5},1..100);
