$enrollment_file = shift @ARGV or die;
$debug = 0;
open C, "course_codes" or die "$0: can not open course_codes: $!";
while ($line = <C>) {
	chomp $line;
	$line =~ s/^\s+//;
	$code = substr($line, 0, 8);
	$name = substr($line, 9);
	$course_name{$code} = $name;
	print STDERR "code=’$code’ -> name=’$name’\n" if $debug;
}
open C, "program_codes" or die "$0: can not open program_codes: $!";
while ($line = <C>) {
	chomp $line;
	$line =~ s/^\s+//;
	$code = substr($line, 0, 4);
	$name = substr($line, 5);
	$program_name{$code} = $name;
	print STDERR "code=’$code’ -> name=’$name’\n" if $debug;
}
%seen = ();
while ($line = <>){
	chomp $line;
	($course, $upi, $name, $program) = split /\|/, $line;
	print "($course, $upi)\n";
	push @{$a{$upi}}, $course;
	$enrol{$course}{$program}++; 
	$program =~ s/\/.*//;
	#$name =~ s/(.*), (\S+).*/$2 $1/;
	$name{$upi} = $name;
	print "$upi @{$a{$upi}}\n";
}

@courses = sort keys %enrol;
foreach $course (sort keys %enrol){
	print "$course has\n";
	@p = keys %{$enrol{course}};
	@p = sort {$enrol{$course}{$b} <=> $enrol{$course}{$a}} @p;
	foreach $program (@p){
		print "$enrol{$course}{$program} from $program_names{$program}\n";
		die if !program_name{$program};
	}
}

#foreach $upi (sort keys %a){
#	print "$name{$upi} is taking @{$a{$upi}}\n";
#}

#open F, "<$enrollment_file" or die;
#while (< F>) {
#	($course,$upi,$name) = split /\s*\|\s*/;
#	$course{$upi} .= " $course";
#	$name =~ s/(.*), (.*)/$2 $1/;
#	$name =~ s/ .* / /;
#	$name{$upi} = $name;
#}
#foreach $course (@ARGV) {
#	foreach $upi (keys %course) {
#		$courses = $course{$upi};
#		next if $courses !~ /$course/;
#		print "$name{$upi} is taking:\n";
#		$courses =~ s/^ //;
#		@courses = split / /, $courses;
#		foreach $course (@courses) {
#			print "$course $course_name{$course}\n";
#		}
#	}
#}
