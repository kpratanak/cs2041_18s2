#! /usr/bin/perl -w

sub selected($\@){
	my($select_expression,  @listref) = @_;
	my (@selected);
	local ($_);
	foreach $_ (@listref){
		push @selected, $_ if &$select_expression; #the & tells perl to execute the expression
	}
	return @selected;
#	foreach $n (@numbers){
#		print "$n\n" if $n % 2 == 1;
#	}

#	or
#	$_% 2 and print "$_\n" foreach @_;

}

#print join (";", selected(sub{$_ > 5},1..100)), "\n";

#print join(";", grep ({$_ >5} 1..100)), "\n";
#@n = (10..20);
#grep {$_ /= 2} @n;
#print "@n\n";

#print join(";", grep ({$_ >5} 1..100)), "\n";
#my @n = (10..20);
#@b = map {$_ *2} @n;
#print "@b\n";

print join(";", grep ({$_ >5} 1..100)), "\n";
my @n = (1..10);
grep {$_ = 42} @n;
print "@n\n";

#print_selected(sub{$_ % 2 == 1},1..100);
#print_selected(sub{$_ >5},1..100);
