#! /usr/bin/perl -w

sub sum {
	my(@nums) = @_;
	my $s = join "+", @nums;
	return $s;
}

print sum(1..1000), "\n";
