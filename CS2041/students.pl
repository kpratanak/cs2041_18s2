#! /usr/bin/perl -w

%course_names = ();
open F, "<course_codes" or die "$0 : can not open course_codes : $!";
while ($line = <F>){
	chomp $line;
	$line =~ /([^ ]+) (.+)/ or
		die  "$0 : bad line format '$line'";
	$course_names{$1} = $2;

}
close F;

while ( $line <>){
	chomp $line;
	
	# @f = split /\|/, $line;
	# $course = $f[0];
	# $enr{$course}++;


	#another way
	# $line =~ /^(\w{4}\d{4})/ or die;
	# or
	# $line =~ /([^\|]\|)/ or die;
	#or
	# $line =~ s/\|.*//;
	# $enr{$&}++;

	#another way
	$course = substr $line, 0, 8;
	$line =~ /\|[a-z]+\, /i;
	$fname = $&;
	$enr{$course}++;
}

foreach $fname (sort keys %enr){
	printf "%s has %4d students\n", $course_names {$course}, $enr{$course} ;
	print "%4d students have first name %s\n", $count{$fname}, $fname;
}
