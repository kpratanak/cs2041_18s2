#! /usr/bin/perl -w

use IO::Socket;
foreach $url (@ARGV){
	$url =~ /http:\/\/([^\/]+)(:(\d+))?(.*)/ or die;
	$c = IO::Socket::INET->new (PeerAddr => $1, PeerPort => $2 || 80) or die;
	# Send request for web page to server
	print $c "Get $4 HTTP/1.0\n\n";
	# Read what the server returns;
	my @webpage = <$c>;
	close $c;
	print "Get $url =>\n", @webpage, "\n";
	
}
