#! /usr/bin/perl -w

use IO::Socket;
$server = IO::Socket::INET->new(LocalPort => 4242, Listen => SOMAXCONN) or die;
while ($c = $server->accept()){
	my $request = <$c>;
	chomp $request;
	my ($method, %path, $protocol) = split /\s+/,$request;
	print STDERR "path = $path\n";
	print $c "Content-type: text/html\n\n";
	print $c " Erika rocks!\n";
	open F, $request;
	print $c <F>;
	close $c;
}
