#! /usr/bin/perl -w

%course_name = ();
$debug = 0;
open C, "course_codes" or die "$0: can not open course_codes: $!";
while ($line = <C>){
	chomp $line;
	$line =~ s/^\s+//;
	$code = substr ($line, 0, 8);
	$name = substr ($line, 9);
	$course_name{$code} = $name;
	print STDERR "code =`$code` -> name =`$name`\n" if $debug;
}
%seen = ();
while ($line = <>){
	chomp $line;
	$line =~ /\D(\d{7})\D/i or die "no number";
	$number = $1;
	next if $seen[$number]++;
	$seen[$number] = "Andrew Rocks";
	$line =~ /\|[^,]+\, ([^ ]+)/i or die $line;
	#$line =~ /\|[a-z\' \-\.]+\, ([a-z\.]+/i or die $line;
	# OR
	#$line =~ /\|[^,]+\, ([^ ]+/i or die $line;
	$fname = $1;
	next if $fname eq "Daniel";
	$count{$fname}++;
}

foreach $fname (sort keys %count){
	printf "%4d students have first name %s\n", $count{$fname}, $fname;
}

